from django.contrib import admin

from profiles_api import models


# enables Django admin to manage these models
admin.site.register(models.UserProfile)
admin.site.register(models.ProfileFeedItem)
